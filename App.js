import * as React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import Constants from 'expo-constants';
import { Audio } from 'expo-av';

export default class App extends React.Component {
    componentDidMount() {
        this.playMySound();
    }

    recording = null;

    playMySound = async () => {
        await Audio.setAudioModeAsync({
            allowsRecordingIOS: true,
            playsInSilentModeIOS: true,
            interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
            staysActiveInBackground: true,
            interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
            shouldDuckAndroid: true,
            playThroughEarpieceAndroid: true,
        });

        this.recording = new Audio.Sound();
        try {
            console.log('here');
            await this.recording.loadAsync(require('./assets/pixies.mp3'));
            await this.recording.playAsync();
            // Your sound is playing!
        } catch (error) {
            console.log('error: ' + error);
            // An error occurred!
        }
    };

    getStatus = async () => {
        try {
            let status = await this.recording.getStatusAsync();
            console.log(status);
        } catch (e) {
            console.log(e);
        }
    };
    render() {
        return (
                <View style={styles.container}>
                    <Text style={styles.paragraph}>
                        Change code in the editor and watch it change on your phone! Save to
                        get a shareable url.
                    </Text>
                    <Button title="get status" onPress={this.getStatus} />
                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
    paragraph: {
        margin: 24,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
});
